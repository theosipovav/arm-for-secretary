﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace arm_secretary
{
    /// <summary>
    /// Класс модели документа
    /// </summary>
    public class ModelDoc
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime create_dt { get; set; }
        /// <summary>
        /// Дата последнего редактирования
        /// </summary>
        public DateTime update_dt { get; set; }
        /// <summary>
        /// Заголовок письма
        /// </summary>
        public string title { get; set; }
        /// <summary>
        /// Содержимое письма
        /// </summary>
        public string body { get; set; }
        /// <summary>
        /// Кому отправляется письмо
        /// </summary>
        public int user_id_to { get; set; }
        /// <summary>
        /// От кого отправляется пиьмо
        /// </summary>
        public int user_from_to { get; set; }
    }
}
