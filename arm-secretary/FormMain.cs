﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace arm_secretary
{
    public partial class FormMain : Form
    {

        List<ModelOrganization> listOrganizations = new List<ModelOrganization>();
        List<ModelUser> listUsers = new List<ModelUser>();

        public FormMain()
        {
            InitializeComponent();



        }
        private void FormMain_Load(object sender, EventArgs e)
        {

            updateData();

        }

        private void buttonUsers_Click(object sender, EventArgs e)
        {
            FormUsers formUsers = new FormUsers();
            formUsers.ShowDialog();
            updateData();
        }


        private void buttonOrganization_Click(object sender, EventArgs e)
        {
            FormOrganization formOrganization = new FormOrganization();
            formOrganization.ShowDialog();
            updateData();
        }


        /// <summary>
        /// Обнолвение данных
        /// </summary>
        private void updateData()
        {
            string sql = "SELECT id as 'ID письма', create_dt as 'Дата создания', update_dt as 'Дата редактирования', title as 'Заголовок', user_id_to as 'Получатель', user_from_to as 'Отправитель' FROM docs";
            using (SqlConnection connection = new SqlConnection(Program.SettingApp.connectionString))
            {
                connection.Open();
                // Создаем объект DataAdapter
                SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
                // Создаем объект Dataset
                DataSet ds = new DataSet();
                // Заполняем Dataset
                adapter.Fill(ds);
                // Отображаем данные
                dataGridViewDocs.DataSource = ds.Tables[0];
            }
            dataGridViewDocs.Refresh();

            // Данные дял выпадающего меню
            this.listOrganizations = new List<ModelOrganization>();
            comboBoxOrganizationFrom.Items.Clear();
            comboBoxOrganizationTo.Items.Clear();
            this.listUsers = new List<ModelUser>();

            // Подготавливаем запрос для MSSQL
            sql = "SELECT id, create_dt, update_dt, name FROM organizations";
            using (SqlConnection connection = new SqlConnection(Program.SettingApp.connectionString))
            {
                // Открываем соеденение
                connection.Open();

                // Создаем объект DataAdapter
                SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);

                // Создаем объект Dataset
                DataSet ds = new DataSet();

                // Заполняем таблицу Dataset
                adapter.Fill(ds);

                // Построчно читаем данные из полученной таблицы
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    ModelOrganization o = new ModelOrganization();
                    o.id = Int32.Parse(row[0].ToString());
                    o.create_dt = DateTime.Parse(row[1].ToString());
                    o.update_dt = DateTime.Parse(row[2].ToString());
                    o.name = row[3].ToString();
                    listOrganizations.Add(o);
                }
                foreach (ModelOrganization o in listOrganizations)
                {
                    comboBoxOrganizationTo.Items.Add(o.name);
                    comboBoxOrganizationFrom.Items.Add(o.name);

                }
            }
            // Подготавливаем запрос для MSSQL
            sql = "SELECT id,create_dt,update_dt,name_first,name_last,organization_id FROM users";
            using (SqlConnection connection = new SqlConnection(Program.SettingApp.connectionString))
            {
                // Открываем соеденение
                connection.Open();

                // Создаем объект DataAdapter
                SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);

                // Создаем объект Dataset
                DataSet ds = new DataSet();

                // Заполняем таблицу Dataset
                adapter.Fill(ds);

                // Построчно читаем данные из полученной таблицы 
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    ModelUser u = new ModelUser();
                    u.id = Int32.Parse(row[0].ToString());
                    u.create_dt = DateTime.Parse(row[1].ToString());
                    u.update_dt = DateTime.Parse(row[2].ToString());
                    u.name_first = row[3].ToString();
                    u.name_last = row[4].ToString();
                    u.organization_id = Int32.Parse(row[5].ToString());
                    this.listUsers.Add(u);

                }
            }

        }

        private void buttonCreate_Click(object sender, EventArgs e)
        {
            FormDocForm formDocForm = new FormDocForm();
            formDocForm.ShowDialog();
            updateData();
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {

            int index = dataGridViewDocs.CurrentCell.RowIndex;
            DataGridViewRow row = dataGridViewDocs.Rows[index];
            int idDoc = (int)row.Cells[0].Value;
            FormDocForm formDocForm = new FormDocForm(idDoc);
            formDocForm.ShowDialog();
            updateData();

        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {

            try
            {
                if (MessageBox.Show("Удалить документ?", "Внимание", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    int index = dataGridViewDocs.CurrentCell.RowIndex;
                    DataGridViewRow row = dataGridViewDocs.Rows[index];
                    int idDoc = (int)row.Cells[0].Value;

                    using (SqlConnection connection = new SqlConnection(Program.SettingApp.connectionString))
                    {
                        string sql = "DELETE FROM docs WHERE id = @id";
                        using (SqlCommand querySaveStaff = new SqlCommand(sql))
                        {
                            querySaveStaff.Connection = connection;
                            querySaveStaff.Parameters.Add("@id", SqlDbType.Int).Value = idDoc;
                            connection.Open();
                            querySaveStaff.ExecuteNonQuery();
                        }
                    }
                    updateData();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
        /// <summary>
        /// Фильтр отправитель
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonFiltr_Click(object sender, EventArgs e)
        {
            if (comboBoxOrganizationFrom.Text == "")
            {
                return;
            }
            int idOrganization = listOrganizations.First(x => x.name == comboBoxOrganizationFrom.Text).id;
            if (listUsers.FirstOrDefault(x => x.organization_id == idOrganization) != null)
            {
                int idUserFrom = listUsers.FirstOrDefault(x => x.organization_id == idOrganization).id;

                string f = "(";
                foreach (ModelUser u in listUsers.Where(x => x.organization_id == idOrganization))
                {
                    f += u.id + ",";
                }
                f += "0)";

                string sql = String.Format("SELECT d.id as 'ID письма', d.create_dt as 'Дата создания', d.update_dt as 'Дата редактирования', d.title as 'Заголовок', d.user_id_to as 'Получатель', d.user_from_to as 'Отправитель' FROM docs as d, users as u WHERE d.user_from_to = u.id and u.id in {0}", f);
                using (SqlConnection connection = new SqlConnection(Program.SettingApp.connectionString))
                {
                    connection.Open();
                    // Создаем объект DataAdapter
                    SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
                    // Создаем объект Dataset
                    DataSet ds = new DataSet();
                    // Заполняем Dataset
                    adapter.Fill(ds);
                    // Отображаем данные
                    dataGridViewDocs.DataSource = ds.Tables[0];
                }
                dataGridViewDocs.Refresh();
            }

        }

        private void buttonResetfilter_Click(object sender, EventArgs e)
        {
            updateData();
        }

        /// <summary>
        /// Фильтр - получатель
        /// </summary>
        private void buttonFiltr2_Click(object sender, EventArgs e)
        {
            if (comboBoxOrganizationTo.Text == "")
            {
                return;
            }
            int idOrganization = listOrganizations.First(x => x.name == comboBoxOrganizationTo.Text).id;

            string f = "(";
            foreach (ModelUser u in listUsers.Where(x => x.organization_id == idOrganization))
            {
                f += u.id + ",";
            }
            f += "0)";

            if (listUsers.FirstOrDefault(x => x.organization_id == idOrganization) != null)
            {
                int idUserFrom = listUsers.FirstOrDefault(x => x.organization_id == idOrganization).id;
                string sql = String.Format("SELECT d.id as 'ID письма', d.create_dt as 'Дата создания', d.update_dt as 'Дата редактирования', d.title as 'Заголовок', d.user_id_to as 'Получатель', d.user_from_to as 'Отправитель' FROM docs as d, users as u WHERE d.user_id_to = u.id and u.id in {0}", f);
                using (SqlConnection connection = new SqlConnection(Program.SettingApp.connectionString))
                {
                    connection.Open();
                    // Создаем объект DataAdapter
                    SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
                    // Создаем объект Dataset
                    DataSet ds = new DataSet();
                    // Заполняем Dataset
                    adapter.Fill(ds);
                    // Отображаем данные
                    dataGridViewDocs.DataSource = ds.Tables[0];
                }
                dataGridViewDocs.Refresh();
            }
        }
    }
}
