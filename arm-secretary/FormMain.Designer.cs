﻿namespace arm_secretary
{
    partial class FormMain
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewDocs = new System.Windows.Forms.DataGridView();
            this.labelTitle = new System.Windows.Forms.Label();
            this.buttonOrganization = new System.Windows.Forms.Button();
            this.buttonRefUsers = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.comboBoxOrganizationTo = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxOrganizationFrom = new System.Windows.Forms.ComboBox();
            this.buttonFiltr1 = new System.Windows.Forms.Button();
            this.buttonResetfilter = new System.Windows.Forms.Button();
            this.buttonFiltr2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDocs)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewDocs
            // 
            this.dataGridViewDocs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewDocs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDocs.Location = new System.Drawing.Point(12, 71);
            this.dataGridViewDocs.Name = "dataGridViewDocs";
            this.dataGridViewDocs.Size = new System.Drawing.Size(775, 583);
            this.dataGridViewDocs.TabIndex = 0;
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelTitle.Location = new System.Drawing.Point(12, 9);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(209, 31);
            this.labelTitle.TabIndex = 1;
            this.labelTitle.Text = "АРМ секретаря";
            // 
            // buttonOrganization
            // 
            this.buttonOrganization.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOrganization.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOrganization.Location = new System.Drawing.Point(793, 71);
            this.buttonOrganization.Name = "buttonOrganization";
            this.buttonOrganization.Size = new System.Drawing.Size(292, 39);
            this.buttonOrganization.TabIndex = 2;
            this.buttonOrganization.Text = "Справочник организаций";
            this.buttonOrganization.UseVisualStyleBackColor = true;
            this.buttonOrganization.Click += new System.EventHandler(this.buttonOrganization_Click);
            // 
            // buttonRefUsers
            // 
            this.buttonRefUsers.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRefUsers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRefUsers.Location = new System.Drawing.Point(793, 116);
            this.buttonRefUsers.Name = "buttonRefUsers";
            this.buttonRefUsers.Size = new System.Drawing.Size(292, 39);
            this.buttonRefUsers.TabIndex = 3;
            this.buttonRefUsers.Text = "Справочник пользователей";
            this.buttonRefUsers.UseVisualStyleBackColor = true;
            this.buttonRefUsers.Click += new System.EventHandler(this.buttonUsers_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(12, 43);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(106, 23);
            this.button3.TabIndex = 4;
            this.button3.Text = "Создать документ";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.buttonCreate_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(124, 43);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(106, 23);
            this.button4.TabIndex = 5;
            this.button4.Text = "Отредактировать";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.buttonEdit_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(236, 42);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(106, 23);
            this.button5.TabIndex = 6;
            this.button5.Text = "Удалить";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // comboBoxOrganizationTo
            // 
            this.comboBoxOrganizationTo.FormattingEnabled = true;
            this.comboBoxOrganizationTo.Location = new System.Drawing.Point(876, 293);
            this.comboBoxOrganizationTo.Name = "comboBoxOrganizationTo";
            this.comboBoxOrganizationTo.Size = new System.Drawing.Size(209, 21);
            this.comboBoxOrganizationTo.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(794, 301);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Получатель";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(793, 191);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(192, 20);
            this.label2.TabIndex = 9;
            this.label2.Text = "Фильтр по организации";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(794, 218);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Отправитель:";
            // 
            // comboBoxOrganizationFrom
            // 
            this.comboBoxOrganizationFrom.FormattingEnabled = true;
            this.comboBoxOrganizationFrom.Location = new System.Drawing.Point(876, 215);
            this.comboBoxOrganizationFrom.Name = "comboBoxOrganizationFrom";
            this.comboBoxOrganizationFrom.Size = new System.Drawing.Size(213, 21);
            this.comboBoxOrganizationFrom.TabIndex = 10;
            // 
            // buttonFiltr1
            // 
            this.buttonFiltr1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonFiltr1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFiltr1.Location = new System.Drawing.Point(797, 320);
            this.buttonFiltr1.Name = "buttonFiltr1";
            this.buttonFiltr1.Size = new System.Drawing.Size(292, 39);
            this.buttonFiltr1.TabIndex = 12;
            this.buttonFiltr1.Text = "Фильтр";
            this.buttonFiltr1.UseVisualStyleBackColor = true;
            this.buttonFiltr1.Click += new System.EventHandler(this.buttonFiltr2_Click);
            // 
            // buttonResetfilter
            // 
            this.buttonResetfilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonResetfilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonResetfilter.Location = new System.Drawing.Point(797, 615);
            this.buttonResetfilter.Name = "buttonResetfilter";
            this.buttonResetfilter.Size = new System.Drawing.Size(292, 39);
            this.buttonResetfilter.TabIndex = 13;
            this.buttonResetfilter.Text = "Сброс фильтра";
            this.buttonResetfilter.UseVisualStyleBackColor = true;
            this.buttonResetfilter.Click += new System.EventHandler(this.buttonResetfilter_Click);
            // 
            // buttonFiltr2
            // 
            this.buttonFiltr2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonFiltr2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFiltr2.Location = new System.Drawing.Point(797, 242);
            this.buttonFiltr2.Name = "buttonFiltr2";
            this.buttonFiltr2.Size = new System.Drawing.Size(292, 39);
            this.buttonFiltr2.TabIndex = 14;
            this.buttonFiltr2.Text = "Фильтр";
            this.buttonFiltr2.UseVisualStyleBackColor = true;
            this.buttonFiltr2.Click += new System.EventHandler(this.buttonFiltr_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1097, 666);
            this.Controls.Add(this.buttonFiltr2);
            this.Controls.Add(this.buttonResetfilter);
            this.Controls.Add(this.buttonFiltr1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBoxOrganizationFrom);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxOrganizationTo);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.buttonRefUsers);
            this.Controls.Add(this.buttonOrganization);
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.dataGridViewDocs);
            this.Name = "FormMain";
            this.Text = "АРМ секретаря";
            this.Load += new System.EventHandler(this.FormMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDocs)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewDocs;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Button buttonOrganization;
        private System.Windows.Forms.Button buttonRefUsers;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.ComboBox comboBoxOrganizationTo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxOrganizationFrom;
        private System.Windows.Forms.Button buttonFiltr1;
        private System.Windows.Forms.Button buttonResetfilter;
        private System.Windows.Forms.Button buttonFiltr2;
    }
}

