﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace arm_secretary
{

    public class ModelOrganization
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Дата создание
        /// </summary>
        public DateTime create_dt { get; set; }

        /// <summary>
        /// Дата Обновления
        /// </summary>
        public DateTime update_dt { get; set; }

        /// <summary>
        /// Наименование организации
        /// </summary>
        public string name { get; set; }
    }
}
