﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace arm_secretary
{
    public partial class FormOrganization : Form
    {
        public FormOrganization()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Обновление данных
        /// </summary>
        private void updateData()
        {
            string sql = "SELECT id as 'ID',name as 'наименование' FROM organizations";
            using (SqlConnection connection = new SqlConnection(Program.SettingApp.connectionString))
            {
                // Открываем соеденение
                connection.Open();
                // Создаем объект DataAdapter
                SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
                // Создаем объект Dataset
                DataSet ds = new DataSet();
                // Заполняем Dataset
                adapter.Fill(ds);
                // Отображаем данные
                dataGridViewOrganizations.DataSource = ds.Tables[0];
            }
            dataGridViewOrganizations.Refresh();
        }

        /// <summary>
        /// Выход
        /// </summary>
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Добавить организация
        /// </summary>
        private void buttonCreate_Click(object sender, EventArgs e)
        {
            FormOrganizationForm formOrganizationForm = new FormOrganizationForm();
            formOrganizationForm.ShowDialog();
            updateData();
        }
        /// <summary>
        /// Редактировать организацию
        /// </summary>
        private void buttonEdit_Click(object sender, EventArgs e)
        {
            int index = dataGridViewOrganizations.CurrentCell.RowIndex;
            DataGridViewRow row = dataGridViewOrganizations.Rows[index];
            int idOrganization = (int)row.Cells[0].Value;
            FormOrganizationForm formOrganizationForm = new FormOrganizationForm(idOrganization);
            formOrganizationForm.ShowDialog();
            updateData();
        }
        /// <summary>
        /// Удалить организацию
        /// </summary>
        private void buttonRemove_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Удалить организацию?", "Внимание", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    int index = dataGridViewOrganizations.CurrentCell.RowIndex;
                    DataGridViewRow row = dataGridViewOrganizations.Rows[index];
                    int idOrganization = (int)row.Cells[0].Value;
                    using (SqlConnection connection = new SqlConnection(Program.SettingApp.connectionString))
                    {
                        string sql = "DELETE FROM organizations WHERE id = @id";
                        using (SqlCommand querySaveStaff = new SqlCommand(sql))
                        {
                            querySaveStaff.Connection = connection;
                            querySaveStaff.Parameters.Add("@id", SqlDbType.Int).Value = idOrganization;
                            connection.Open();
                            querySaveStaff.ExecuteNonQuery();
                        }
                    }
                    updateData();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        }

        private void FormOrganization_Load(object sender, EventArgs e)
        {
            updateData();
        }
    }
}
