﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace arm_secretary
{
    public partial class FormUsersForm : Form
    {
        List<ModelOrganization> listOrganizations = new List<ModelOrganization>();

        ModelUser user = null;
        bool isCreate;


        public FormUsersForm()
        {
            InitializeComponent();
            isCreate = true;
        }
        public FormUsersForm(int idUser)
        {
            InitializeComponent();

            // Заполнение поле, если выбран ренжим редактирование
            this.user = new ModelUser();
            
            // Подготавливаем запрос для MSSQL
            string sql = String.Format("SELECT TOP (1) id,create_dt,update_dt,name_first,name_last,organization_id FROM users WHERE id = {0}", idUser);
            using (SqlConnection connection = new SqlConnection(Program.SettingApp.connectionString))
            {
                connection.Open();   // Открываем соеденение
                SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);   // Создаем объект DataAdapter
                DataSet ds = new DataSet(); // Создаем объект Dataset
                adapter.Fill(ds);   // Заполняем таблицу Dataset

                // Построчно читаем данные из полученной таблицы
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    this.user.id = Int32.Parse(row[0].ToString());
                    this.user.create_dt = DateTime.Parse(row[1].ToString());
                    this.user.update_dt = DateTime.Parse(row[2].ToString());
                    this.user.name_first = row[3].ToString();
                    this.user.name_last = row[4].ToString();
                    this.user.organization_id = Int32.Parse(row[5].ToString());
                    break;
                }
            }
        }


        /// <summary>
        /// Событие возникающее перед загрузкой формы
        /// загружаем данные в форму
        /// </summary>
        private void FormUsersAdd_Load(object sender, EventArgs e)
        {
            // Подготавливаем запрос для MSSQL
            string sql = "SELECT id, create_dt, update_dt, name FROM organizations";
            using (SqlConnection connection = new SqlConnection(Program.SettingApp.connectionString))
            {
                // Открываем соеденение
                connection.Open();

                // Создаем объект DataAdapter
                SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);

                // Создаем объект Dataset
                DataSet ds = new DataSet();

                // Заполняем таблицу Dataset
                adapter.Fill(ds);

                // Построчно читаем данные из полученной таблицы
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    ModelOrganization o = new ModelOrganization();
                    o.id = Int32.Parse(row[0].ToString());
                    o.create_dt = DateTime.Parse(row[1].ToString());
                    o.update_dt = DateTime.Parse(row[2].ToString());
                    o.name = row[3].ToString();
                    listOrganizations.Add(o);
                }
                foreach (ModelOrganization o in listOrganizations)
                {
                    comboBoxOrganization.Items.Add(o.name);

                }
            }

            if (this.user != null)
            {
                textBoxNameFirst.Text = this.user.name_first;
                textBoxNameLast.Text = this.user.name_last;
                comboBoxOrganization.Text = (listOrganizations.First(x => x.id == this.user.organization_id)).name;
            }



        }

        /// <summary>
        /// Выход
        /// </summary>
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (textBoxNameLast.Text == "")
            {
                MessageBox.Show("Поле Фамилия не заполнено;", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (textBoxNameFirst.Text == "")
            {
                MessageBox.Show("Поле Инициалы не заполнено;", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (comboBoxOrganization.Text == "")
            {
                MessageBox.Show("Поле Организация не заполнено;", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            try
            {
                if (this.isCreate)
                {
                    createUser();
                }
                else
                {
                    updateUser();
                }
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        }


        /// <summary>
        /// Создание нового пользователя
        /// </summary>
        private void createUser()
        {
            using (SqlConnection connection = new SqlConnection(Program.SettingApp.connectionString))
            {
                string sql = "INSERT users (create_dt,update_dt,name_first,name_last,organization_id) VALUES (@create_dt,@update_dt,@name_first,@name_last,@organization_id)";
                using (SqlCommand querySaveStaff = new SqlCommand(sql))
                {
                    querySaveStaff.Connection = connection;
                    querySaveStaff.Parameters.Add("@create_dt", SqlDbType.DateTime).Value = DateTime.Now;
                    querySaveStaff.Parameters.Add("@update_dt", SqlDbType.DateTime).Value = DateTime.Now;
                    querySaveStaff.Parameters.Add("@name_first", SqlDbType.VarChar, 100).Value = textBoxNameFirst.Text;
                    querySaveStaff.Parameters.Add("@name_last", SqlDbType.VarChar, 100).Value = textBoxNameLast.Text;
                    querySaveStaff.Parameters.Add("@organization_id", SqlDbType.Int).Value = (listOrganizations.First(x => x.name == comboBoxOrganization.Text)).id;
                    connection.Open();
                    querySaveStaff.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Обновление данных пользователя
        /// </summary>
        private void updateUser()
        {
            using (SqlConnection connection = new SqlConnection(Program.SettingApp.connectionString))
            {
                string sql = "UPDATE users SET update_dt = @update_dt, name_first = @name_first, name_last = @name_last, organization_id = @organization_id WHERE id = @id;";
                using (SqlCommand querySaveStaff = new SqlCommand(sql))
                {
                    querySaveStaff.Connection = connection;
                    querySaveStaff.Parameters.Add("@id", SqlDbType.Int).Value = this.user.id;
                    querySaveStaff.Parameters.Add("@update_dt", SqlDbType.DateTime).Value = DateTime.Now;
                    querySaveStaff.Parameters.Add("@name_first", SqlDbType.VarChar, 100).Value = textBoxNameFirst.Text;
                    querySaveStaff.Parameters.Add("@name_last", SqlDbType.VarChar, 100).Value = textBoxNameLast.Text;
                    querySaveStaff.Parameters.Add("@organization_id", SqlDbType.Int).Value = (listOrganizations.First(x => x.name == comboBoxOrganization.Text)).id;
                    connection.Open();
                    querySaveStaff.ExecuteNonQuery();
                }
            }

        }




    }
}
