﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace arm_secretary
{
    /// <summary>
    /// Класс модели пользовтаеля
    /// </summary>
    public class ModelUser
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// Дата создание 
        /// </summary>
        public DateTime create_dt { get; set; }
        /// <summary>
        /// Дата последнего обновления 
        /// </summary>
        public DateTime update_dt { get; set; }
        /// <summary>
        /// Инициалы
        /// </summary>
        public string name_first { get; set; }
        /// <summary>
        /// Фамилия
        /// </summary>
        public string name_last { get; set; }
        /// <summary>
        /// Идентификатор организации
        /// </summary>
        public int organization_id { get; set; }
    }
}
