﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace arm_secretary
{
    public partial class FormUsers : Form
    {
        public FormUsers()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Выход
        /// </summary>
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Событие возникающее перед загрузкой формы
        /// загружаем данные в форму
        /// </summary>
        private void FormUsers_Load(object sender, EventArgs e)
        {
            updateData();
            
        }

        /// <summary>
        /// Сздать
        /// </summary>
        private void buttonCreate_Click(object sender, EventArgs e)
        {
            FormUsersForm formUsersAdd = new FormUsersForm();
            formUsersAdd.ShowDialog();
            updateData();
        }

        /// <summary>
        /// Редактировать
        /// </summary>
        private void buttonEdit_Click(object sender, EventArgs e)
        {
            int index = dataGridViewUsers.CurrentCell.RowIndex;
            DataGridViewRow row =  dataGridViewUsers.Rows[index];
            int idUser = (int)row.Cells[0].Value;
            FormUsersForm formUsersAdd = new FormUsersForm(idUser);
            formUsersAdd.ShowDialog();
            updateData();
        }
        /// <summary>
        /// Удалить
        /// </summary>
        private void buttonRemove_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Удалить пользователя?", "Внимание", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                int index = dataGridViewUsers.CurrentCell.RowIndex;
                DataGridViewRow row = dataGridViewUsers.Rows[index];
                int idUser = (int)row.Cells[0].Value;
                using (SqlConnection connection = new SqlConnection(Program.SettingApp.connectionString))
                {
                    string sql = "DELETE FROM users WHERE id = @id";
                    using (SqlCommand querySaveStaff = new SqlCommand(sql))
                    {
                        querySaveStaff.Connection = connection;
                        querySaveStaff.Parameters.Add("@id", SqlDbType.Int).Value = idUser;
                        connection.Open();
                        querySaveStaff.ExecuteNonQuery();
                    }
                }
                updateData();
            }
        }

        /// <summary>
        /// Обновление данных
        /// </summary>
        private void updateData()
        {
            string sql = "SELECT id as 'N', name_last as 'Фамилия', name_first as 'Инициалы', (SELECT  TOP (1) o.name FROM organizations as o WHERE o.id = u.organization_id ) as 'Компания' FROM users as u;";
            using (SqlConnection connection = new SqlConnection(Program.SettingApp.connectionString))
            {
                // Открываем соеденение
                connection.Open();
                // Создаем объект DataAdapter
                SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
                // Создаем объект Dataset
                DataSet ds = new DataSet();
                // Заполняем Dataset
                adapter.Fill(ds);
                // Отображаем данные
                dataGridViewUsers.DataSource = ds.Tables[0];
            }
            dataGridViewUsers.Refresh();
        }
    }
}
