﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace arm_secretary
{
    public partial class FormOrganizationForm : Form
    {
        /// <summary>
        /// Класс для организации
        /// </summary>
        ModelOrganization organization = null;

        /// <summary>
        /// Флаг создание
        /// </summary>
        bool isCreate;

        /// <summary>
        /// Создать новую организацию
        /// </summary>
        public FormOrganizationForm()
        {
            InitializeComponent();
            isCreate = true;
        }
        /// <summary>
        /// Редактирование организации
        /// </summary>
        public FormOrganizationForm(int idOrganization)
        {
            InitializeComponent();
            isCreate = false;
            organization = new ModelOrganization();

            // Подготавливаем запрос для MSSQL
            string sql = String.Format("SELECT TOP (1) id,create_dt,update_dt,name FROM organizations WHERE id = {0}", idOrganization);
            using (SqlConnection connection = new SqlConnection(Program.SettingApp.connectionString))
            {
                connection.Open();   // Открываем соеденение
                SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);   // Создаем объект DataAdapter
                DataSet ds = new DataSet(); // Создаем объект Dataset
                adapter.Fill(ds);   // Заполняем таблицу Dataset

                // Построчно читаем данные из полученной таблицы
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    this.organization.id = Int32.Parse(row[0].ToString());
                    this.organization.create_dt = DateTime.Parse(row[1].ToString());
                    this.organization.update_dt = DateTime.Parse(row[2].ToString());
                    this.organization.name = row[3].ToString();
                    break;
                }
            }
        }
        /// <summary>
        /// Сохранить
        /// </summary>
        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (textBoxName.Text == "")
            {
                MessageBox.Show("Поле наименование не заполнено;", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }


            try
            {
                if (this.isCreate)
                {
                    createOrganization();
                }
                else
                {
                    updateOrganization();
                }
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
        /// <summary>
        /// Выход
        /// </summary>
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Событие возникающее перед загрузкой формы
        /// загружаем данные в форму
        /// </summary>
        private void FormOrganizationForm_Load(object sender, EventArgs e)
        {
            if (this.organization != null)
            {
                textBoxName.Text = this.organization.name;
            }
        }


        /// <summary>
        /// Создание новой организации
        /// </summary>
        private void createOrganization()
        {
            using (SqlConnection connection = new SqlConnection(Program.SettingApp.connectionString))
            {
                string sql = "INSERT INTO organizations (create_dt,update_dt,name) VALUES (@create_dt,@update_dt,@name)";
                using (SqlCommand querySaveStaff = new SqlCommand(sql))
                {
                    querySaveStaff.Connection = connection;
                    querySaveStaff.Parameters.Add("@create_dt", SqlDbType.DateTime).Value = DateTime.Now;
                    querySaveStaff.Parameters.Add("@update_dt", SqlDbType.DateTime).Value = DateTime.Now;
                    querySaveStaff.Parameters.Add("@name", SqlDbType.VarChar, 100).Value = textBoxName.Text;
                    connection.Open();
                    querySaveStaff.ExecuteNonQuery();
                }
            }
        }



        /// <summary>
        /// Редактирование организации
        /// </summary>
        private void updateOrganization()
        {
            using (SqlConnection connection = new SqlConnection(Program.SettingApp.connectionString))
            {
                string sql = "UPDATE organizations SET [update_dt] = '@update_dt',[name] = '@name' WHERE id = '@id'";
                using (SqlCommand querySaveStaff = new SqlCommand(sql))
                {
                    querySaveStaff.Connection = connection;
                    querySaveStaff.Parameters.Add("@id", SqlDbType.Int).Value = this.organization.id;
                    querySaveStaff.Parameters.Add("@update_dt", SqlDbType.DateTime).Value = DateTime.Now;
                    querySaveStaff.Parameters.Add("@name", SqlDbType.VarChar, 150).Value = textBoxName.Text;
                    connection.Open();
                    querySaveStaff.ExecuteNonQuery();
                }
            }
        }

    }
}
