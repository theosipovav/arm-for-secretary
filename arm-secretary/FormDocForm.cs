﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace arm_secretary
{
    public partial class FormDocForm : Form
    {
        List<ModelUser> listUsers = new List<ModelUser>();

        // Модель документа 
        ModelDoc doc = null;

        // Флаг на создание нового документа
        bool isCreate;

        public FormDocForm()
        {
            InitializeComponent();
            isCreate = true;
        }
        public FormDocForm(int idDoc)
        {
            InitializeComponent();
            isCreate = false;

            // Заполнение поле, если выбран ренжим редактирование
            this.doc = new ModelDoc();

            // Подготавливаем запрос для MSSQL
            string sql = String.Format("SELECT id,create_dt,update_dt,title,body,user_id_to,user_from_to FROM docs WHERE id = {0}", idDoc);
            using (SqlConnection connection = new SqlConnection(Program.SettingApp.connectionString))
            {
                connection.Open();   // Открываем соеденение
                SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);   // Создаем объект DataAdapter
                DataSet ds = new DataSet(); // Создаем объект Dataset
                adapter.Fill(ds);   // Заполняем таблицу Dataset

                // Построчно читаем данные из полученной таблицы
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    this.doc.id = Int32.Parse(row[0].ToString());
                    this.doc.create_dt = DateTime.Parse(row[1].ToString());
                    this.doc.update_dt = DateTime.Parse(row[2].ToString());
                    this.doc.title = row[3].ToString();
                    this.doc.body = row[4].ToString();
                    this.doc.user_id_to = Int32.Parse(row[5].ToString());
                    this.doc.user_from_to = Int32.Parse(row[6].ToString());
                    break;
                }
            }

        }

        /// <summary>
        /// Событие возникающее перед загрузкой формы
        /// загружаем данные в форму
        /// </summary>
        private void FormDocForm_Load(object sender, EventArgs e)
        {
            // Подготавливаем запрос для MSSQL
            string sql = "SELECT id,create_dt,update_dt,name_first,name_last,organization_id FROM users";
            using (SqlConnection connection = new SqlConnection(Program.SettingApp.connectionString))
            {
                // Открываем соеденение
                connection.Open();

                // Создаем объект DataAdapter
                SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);

                // Создаем объект Dataset
                DataSet ds = new DataSet();

                // Заполняем таблицу Dataset
                adapter.Fill(ds);

                // Построчно читаем данные из полученной таблицы 
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    ModelUser u = new ModelUser();
                    u.id = Int32.Parse(row[0].ToString());
                    u.create_dt = DateTime.Parse(row[1].ToString());
                    u.update_dt = DateTime.Parse(row[2].ToString());
                    u.name_first = row[3].ToString();
                    u.name_last = row[4].ToString();
                    u.organization_id = Int32.Parse(row[5].ToString());
                    this.listUsers.Add(u);

                }
                foreach (ModelUser u in listUsers)
                {
                    comboBoxFrom.Items.Add(String.Format("{0} {1}", u.name_last, u.name_first));
                    comboBoxTo.Items.Add(String.Format("{0} {1}", u.name_last, u.name_first));

                }
            }

            if (this.doc != null)
            {
                textBoxTitle.Text = this.doc.title;
                textBoxBody.Text = this.doc.body;

                comboBoxFrom.Text = String.Format("{0} {1}", listUsers.First(x => x.id == this.doc.user_from_to).name_last, listUsers.First(x => x.id == this.doc.user_from_to).name_first);
                comboBoxTo.Text = String.Format("{0} {1}", listUsers.First(x => x.id == this.doc.user_from_to).name_last, listUsers.First(x => x.id == this.doc.user_from_to).name_first);

            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (textBoxTitle.Text == "")
            {
                MessageBox.Show("Поле Заголовок не заполнено;", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (textBoxBody.Text == "")
            {
                MessageBox.Show("Поле Содержимое письма не заполнено;", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (comboBoxFrom.Text == "")
            {
                MessageBox.Show("Поле От кого не заполнено;", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (comboBoxTo.Text == "")
            {
                MessageBox.Show("Поле Кому не заполнено;", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            try
            {
                if (this.isCreate)
                {
                    createDoc();
                }
                else
                {
                    updateDoc();
                }
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }


        /// <summary>
        /// Создание нового документа
        /// </summary>
        private void createDoc()
        {
            using (SqlConnection connection = new SqlConnection(Program.SettingApp.connectionString))
            {
                string sql = "INSERT INTO docs(create_dt,update_dt,title,body,user_id_to,user_from_to) VALUES (@create_dt,@update_dt,@title,@body,@user_id_to,@user_from_to)";
                using (SqlCommand querySaveStaff = new SqlCommand(sql))
                {
                    querySaveStaff.Connection = connection;
                    querySaveStaff.Parameters.Add("@create_dt", SqlDbType.DateTime).Value = DateTime.Now;
                    querySaveStaff.Parameters.Add("@update_dt", SqlDbType.DateTime).Value = DateTime.Now;
                    querySaveStaff.Parameters.Add("@title", SqlDbType.VarChar, 150).Value = textBoxTitle.Text;
                    querySaveStaff.Parameters.Add("@body", SqlDbType.Text).Value = textBoxBody.Text;
                    querySaveStaff.Parameters.Add("@user_id_to", SqlDbType.Int).Value = listUsers.First(x => String.Format("{0} {1}", x.name_last, x.name_first) == comboBoxTo.Text).id;
                    querySaveStaff.Parameters.Add("@user_from_to", SqlDbType.Int).Value = listUsers.First(x => String.Format("{0} {1}", x.name_last, x.name_first) == comboBoxFrom.Text).id;
                    connection.Open();
                    querySaveStaff.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Обновление документа
        /// </summary>
        private void updateDoc()
        {
            using (SqlConnection connection = new SqlConnection(Program.SettingApp.connectionString))
            {
                string sql = "UPDATE docs SET [update_dt] = @update_dt,[title] = @title,[body] = @body,[user_id_to] = @user_id_to,[user_from_to] = @user_from_to WHERE id = @id";
                using (SqlCommand querySaveStaff = new SqlCommand(sql))
                {
                    querySaveStaff.Connection = connection;
                    querySaveStaff.Parameters.Add("@id", SqlDbType.Int, 150).Value = this.doc.id;
                    querySaveStaff.Parameters.Add("@update_dt", SqlDbType.DateTime).Value = DateTime.Now;
                    querySaveStaff.Parameters.Add("@title", SqlDbType.VarChar, 150).Value = textBoxTitle.Text;
                    querySaveStaff.Parameters.Add("@body", SqlDbType.VarChar, 150).Value = textBoxBody.Text;
                    querySaveStaff.Parameters.Add("@user_id_to", SqlDbType.Int).Value = listUsers.First(x => String.Format("{0} {1}", x.name_last, x.name_first) == comboBoxTo.Text).id;
                    querySaveStaff.Parameters.Add("@user_from_to", SqlDbType.Int).Value = listUsers.First(x => String.Format("{0} {1}", x.name_last, x.name_first) == comboBoxFrom.Text).id;
                    connection.Open();
                    querySaveStaff.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Выход
        /// </summary>
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
